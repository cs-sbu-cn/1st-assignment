# Computational Neuroscience
### 1st Assignment - Shahid Beheshti University - Bachelor’s Program
March 14, 2023<br/> 
Mahdieh Sajedi Pour, Nima Taheri<br/>

In this part, you will implement a Leaky Integrate and Fire(LIF) model.<br/>

∙ Implement a LIF model with changeable parameters. <br/>
∙ Feed the model with a step shaped current, then plot the current, the voltage, and the F-I curve.<br/>
![alt](https://gitlab.com/cs-sbu-cn/1st-assignment/-/raw/main/currents.png)<br/>
∙ Feed the model with a noisy current, then plot the current, the voltage, and the F-I curve.<br/>
![alt](https://gitlab.com/cs-sbu-cn/1st-assignment/-/raw/main/noise.png)<br/>
∙ Do number 2 and 3 for at least 5 different settings.